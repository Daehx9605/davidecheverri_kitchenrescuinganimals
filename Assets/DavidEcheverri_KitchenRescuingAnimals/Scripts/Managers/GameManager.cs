﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Dish dish;
    public UIManager uIManager;
    private XMLManager xmlManager;
    public int selecRecipe;

    private void Start()
    {
        xmlManager = new XMLManager();
        xmlManager.LoadXML();
        dish.SendFood += OnSendFood;
    }



    private void OnSendFood(GameObject food)
    {

    }
    private void GameState(Game_States states)
    {
        switch (states)
        {
            case Game_States.Main:
                break;
            case Game_States.Tutorial:
                break;
            case Game_States.Selec_Recipe:
                dish.NewRecipe(xmlManager.LoadRecipe(selecRecipe));
                break;
            case Game_States.Gameplay:
                break;
            case Game_States.Score:
                break;
            default:
                break;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            GameState(Game_States.Selec_Recipe);
        }
    }
}
