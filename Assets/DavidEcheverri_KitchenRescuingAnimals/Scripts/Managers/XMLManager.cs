﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

//using utils;

/// <summary>
/// Singlenton Administrador XML
/// </summary>
public class XMLManager
{
    private XmlDocument xmlDocument;
    private TextAsset textXML;

    public List<Ingredient> ingreduentsByRecipe;

    public void LoadXML()
    {
        textXML = Resources.Load("XML/RecipesGame") as TextAsset;
        xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(textXML.ToString());
    }
    public List<Ingredient> LoadRecipe(int recipeValue)
    {
        int numberIngredientes = int.Parse(xmlDocument.DocumentElement.SelectSingleNode("/Recipes/Recipe" + recipeValue + "/NumberIngredient").InnerText);

        ingreduentsByRecipe = new List<Ingredient>();
        ingreduentsByRecipe.Clear();

        Debug.Log("Receta Cargada: " + recipeValue);

        for (int i = 1; i <= numberIngredientes; i++)
        {
            Ingredient ingredient = new Ingredient();
            ingredient.name = (xmlDocument.DocumentElement.SelectSingleNode("/Recipes/Recipe" + recipeValue + "/Ingredient" + i + "/name").InnerText);
            ingredient.stateCut = (xmlDocument.DocumentElement.SelectSingleNode("/Recipes/Recipe" + recipeValue + "/Ingredient" + i + "/CutState").InnerText);
            ingredient.cokedState = (xmlDocument.DocumentElement.SelectSingleNode("/Recipes/Recipe" + recipeValue + "/Ingredient" + i + "/CookedState").InnerText);
            ingreduentsByRecipe.Add(ingredient);
            Debug.Log("Ingredient: " + i + " Name: " + ingreduentsByRecipe[i - 1].name + " stateCut: " + ingreduentsByRecipe[i - 1].stateCut + " ActionTime: " + ingreduentsByRecipe[i - 1].cokedState);
        }
        return ingreduentsByRecipe;
    }
}

