﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SensorDish : MonoBehaviour
{
    public Action<GameObject, bool> Ingredente;
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Food")
        {
            Debug.Log("sale" + other.name);
            Ingredente(other.gameObject, false);
        }
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Food")
        {
            Debug.Log("entra " + other.name);
            Ingredente(other.gameObject, true);
        }
        
    }
}
