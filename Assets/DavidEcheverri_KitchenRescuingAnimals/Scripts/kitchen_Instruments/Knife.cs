﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;

public class Knife : Implements
{
    protected override void OnEnterFood(GameObject _target)
    {
        if(_target.tag == "Food" && _target.GetComponent<Food_States_Base>().Cut_States != Cut_States.Cut_Quarters)
        {
            Parameters parameters = new Parameters();
            parameters.fryAmount = _target.GetComponent<Fry_Base>().AmountFry;
            parameters.pointFry = _target.GetComponent<Fry_Base>().PointFry;
            parameters.fryTimeAmount = _target.GetComponent<Fry_Base>().FryTimeCoked;
            parameters.burnedpointfry = _target.GetComponent<Fry_Base>().PointBurned;
            parameters.delayActionFry = _target.GetComponent<Fry_Base>().DelayAction;
            parameters.boilAmount = _target.GetComponent<Boil_Base>().AmountBoil;
            parameters.pointBoil = _target.GetComponent<Boil_Base>().PonitBoil;
            parameters.boilTimeAmount = _target.GetComponent<Boil_Base>().BoilTimeCoked;
            parameters.burnedpointBoil = _target.GetComponent<Boil_Base>().PointBurned;
            parameters.delayActionBoil = _target.GetComponent<Boil_Base>().DelayAction;
            parameters.cutStateOrigen = _target.GetComponent<Food_States_Base>().Cut_States;

            SlicedHull hull = SliceObject(_target, null);
      
            if (hull != null)
            {
                GameObject botton = hull.CreateLowerHull(_target, null);
                botton.name = _target.name;
                GameObject top = hull.CreateUpperHull(_target, null);
                top.name = _target.name;
                AddHullComponets(botton);
                AddHullComponets(top);
                SetHullComponet(botton, parameters);
                SetHullComponet(top, parameters);
                Destroy(_target);
            }
        }
        
    }
    private SlicedHull SliceObject(GameObject obj, Material croosSectionMaterial = null)
    {
        return obj.Slice(transform.position, transform.up, croosSectionMaterial);
    }

    private void AddHullComponets(GameObject _objetc)
    {
        _objetc.AddComponent<Fry_Base>();
        _objetc.AddComponent<Boil_Base>();
        _objetc.AddComponent<Cut_Base>();
        _objetc.AddComponent<Food_States_Base>();
        _objetc.tag = "Food";
        Rigidbody rB = _objetc.AddComponent<Rigidbody>();
        rB.interpolation = RigidbodyInterpolation.Interpolate;
        MeshCollider collider = _objetc.AddComponent<MeshCollider>();
        collider.convex = true;
    }

    private void SetHullComponet(GameObject _objetc,Parameters parametersEnter)
    {
        _objetc.GetComponent<Fry_Base>().ChangeFryValues(parametersEnter);
        _objetc.GetComponent<Boil_Base>().ChangeBoilValue(parametersEnter);
        _objetc.GetComponent<Food_States_Base>().Cut_States = parametersEnter.cutStateOrigen;
        _objetc.GetComponent<Cut_Base>().ActiveAction();
    }


}
