﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pot : Implements
{
    protected override void OnEnterFood(GameObject _target)
    {
        Debug.Log(_target.name);
        _target.GetComponent<Boil_Base>().ActiveAction();
    }
}
