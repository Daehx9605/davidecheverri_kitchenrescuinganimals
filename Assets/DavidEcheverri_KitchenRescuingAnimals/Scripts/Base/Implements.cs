﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Implements : MonoBehaviour
{
    [SerializeField]
    protected Implement_Use implement_Use;
    [SerializeField]
    protected float timeEjecution;
    [SerializeField]
    protected float timeCur;

    protected virtual void Awake()
    {
        GetComponentInChildren<Sensor>().EnterFood += OnEnterFood;   
    }

    protected virtual void OnEnterFood(GameObject _target)
    {
        
    }

    

}
