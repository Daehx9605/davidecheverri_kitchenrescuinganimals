﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food_Base : MonoBehaviour
{
    [SerializeField]
    protected bool canAction = true;
    [SerializeField]
    protected float delayAction;
    [SerializeField]
    protected float pointBurned;


    public virtual void ActiveAction()
    {
        
    }

    public virtual void EndAction()
    {

    }

    protected virtual IEnumerator WaitNextAction()
    {
        float waitTime = 0;
        while (waitTime < delayAction)
        {
            waitTime += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        canAction = true;
    }

    protected virtual void EvaluateStatus()
    {

    }

    public float DelayAction
    {
        get { return delayAction; }
        set { delayAction = value; }
    }

    public float PointBurned
    {
        get { return pointBurned; }
        set { pointBurned = value; }
    }

}
