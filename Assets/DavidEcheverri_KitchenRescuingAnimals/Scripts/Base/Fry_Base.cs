﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fry_Base : Food_Base
{
    [SerializeField]
    private float fryTimeCoked;
    [SerializeField]
    private float amountFry = 0;
    [SerializeField]
    private float pointFry = 0;

    public override void ActiveAction()
    {      
        if (canAction && GetComponent<Food_States_Base>().Coked_States != Coked_States.Boil)
        {
            Debug.Log("entra");
            amountFry += fryTimeCoked;
            EvaluateStatus();
            canAction = false;
            StartCoroutine(WaitNextAction());
        }
        
    }

    public override void EndAction()
    {
        
    }

    public void ChangeFryValues(Parameters parameters)
    {
        amountFry = parameters.fryAmount;
        fryTimeCoked = parameters.fryTimeAmount;
        pointFry = parameters.pointFry;
        pointBurned = parameters.burnedpointfry;
        delayAction = parameters.delayActionFry;
        EvaluateStatus();
    }

    public float AmountFry
    {
        get {return amountFry; }
    }

    public float PointFry
    {
        get { return pointFry; }
    }

    public float FryTimeCoked
    {
        get { return fryTimeCoked; }
    }

    protected override IEnumerator WaitNextAction()
    {
        return base.WaitNextAction();
    }

    protected override void EvaluateStatus()
    {
        if(amountFry >= pointFry && amountFry < pointBurned)
        {
            GetComponent<Food_States_Base>().Coked_States = Coked_States.Fry;
        }
        if(amountFry >= pointBurned)
        {
            GetComponent<Food_States_Base>().Coked_States = Coked_States.Burned;
        }
    }



}
