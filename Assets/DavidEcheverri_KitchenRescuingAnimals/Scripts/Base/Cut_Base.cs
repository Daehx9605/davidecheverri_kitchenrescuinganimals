﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Cut_Base : Food_Base
{
    public override void ActiveAction()
    {
        switch (GetComponent<Food_States_Base>().Cut_States)
        {
            case Cut_States.Whole:
                GetComponent<Food_States_Base>().Cut_States = Cut_States.Cut_Half;
                break;
            case Cut_States.Cut_Half:
                GetComponent<Food_States_Base>().Cut_States = Cut_States.Cut_Quarters;
                break;
            default:
                break;
        }
    }
}
