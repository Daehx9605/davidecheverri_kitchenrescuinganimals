﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boil_Base : Food_Base
{
    [SerializeField]
    protected float boilTimeCoked = 0;
    [SerializeField]
    protected float amountBoil = 0;
    [SerializeField]
    protected float pointBoil = 0;
    public override void ActiveAction()
    {
        if (canAction && GetComponent<Food_States_Base>().Coked_States != Coked_States.Fry)
        {
            Debug.Log("entra a boil");
            amountBoil += boilTimeCoked;
            EvaluateStatus();
            canAction = false;
            StartCoroutine(WaitNextAction());
        }
    }

    public override void EndAction()
    {
       
    }

    public float AmountBoil
    {
        get { return amountBoil; }
    }

    public float BoilTimeCoked
    {
        get { return boilTimeCoked; }
    }

    public float PonitBoil
    {
        get { return pointBoil; }
    }

    public void ChangeBoilValue(Parameters parametersEnter)
    {
        amountBoil = parametersEnter.boilAmount;
        boilTimeCoked = parametersEnter.boilTimeAmount;
        pointBoil = parametersEnter.pointBoil;
        pointBurned = parametersEnter.burnedpointBoil;
        delayAction = parametersEnter.delayActionBoil;
    }

    protected override void EvaluateStatus()
    {
        if (amountBoil >= pointBoil && amountBoil < pointBurned)
        {
            GetComponent<Food_States_Base>().Coked_States = Coked_States.Boil;
        }
        if (amountBoil >= pointBurned)
        {
            GetComponent<Food_States_Base>().Coked_States = Coked_States.Burned;
        }
    }
}
