﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food_States_Base : MonoBehaviour
{
    [SerializeField]
    private Cut_States cutState = Cut_States.Whole;
    [SerializeField]
    private Coked_States cokedState = Coked_States.Raw;
    private bool inRecipe = false;


    public Coked_States Coked_States
    {
        get
        {
            return cokedState;
        }
        set
        {
            cokedState = value;
        }
    }

    public Cut_States Cut_States
    {
        get
        {
            return cutState;
        }
        set
        {
            cutState = value;
        }
    }

    public bool InRecipe
    {
        get
        {
            return inRecipe;
        }
        set
        {
            inRecipe = value;
        }
    }


}
