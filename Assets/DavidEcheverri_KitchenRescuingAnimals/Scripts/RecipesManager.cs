﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipesManager : MonoBehaviour
{
    [Header("Cancion Asignada")]
    public int assignedSong = 1;


    private XMLManager xmlManager;
    private void Start()
    {
        xmlManager = new XMLManager();
        xmlManager.LoadXML();
    }

    private void LoadRecipe()
    {
        xmlManager.LoadRecipe(assignedSong);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadRecipe();
        }
    }
}
